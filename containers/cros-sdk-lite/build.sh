#!/bin/bash
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Copyright 2023 Ideas On Board Oy
# Jacopo Mondi <jacopo.mondi@ideasonboard.com>

# This script exists only because the Chrome OS SDK can't be built inside an
# unprivileged container, and 'podman build' doesn't support running with
# privileges. The image build process needs to be divided in three stages:
#
# - Stage 1 builds the initial image with the Chrome OS sources using 'podman
#   build'. This stage doesn't require elevated privileges.
# - Stage 2 runs most of the SDK build using a shell script executed in a
#   privileged container, using the image built in stage 1. This is the stage
#   that requires privileges, so it has to be run with manual invocation of
#   'podman container run'. At the end of the run the container is committed as
#   the stage 2 image.
# - Stage 3 builds the final image using 'podman build' and copying file from
#   the stage 2 image.

set -ex

: ${CACHE_OPTS:=--no-cache}
: ${NUM_JOBS:=$(nproc)}

source .env

# Expected name format "release-R123-4567.B" -> "r123-4567"
cros_revision=$(echo ${CROS_SDK_REVISION#release-} | tr '[:upper:]' '[:lower:]')
cros_revision=${cros_revision%.b}

image_name=cros-sdk-lite-${cros_revision}
image_tag=$(date +%F)

# Build the stage 1 image.
echo "Building stage 1 for ${image_name}"

pushd stage1
podman build ${CACHE_OPTS} -t "${image_name}-stage1:latest" \
	--build-arg cros_sdk_revision=${CROS_SDK_REVISION} \
	.
popd

# Build the stage 2 image by running the stage 2 build script in a container
# based on the stage 1 image. Ideally this should be part of a single
# Containerfile covering all stages, but this stage requires running in
# privileged mode, which 'podman build' doesn't support.
echo "Building stage 2 for ${image_name}"

id=$(podman container run -d --privileged \
	--env cros_board=${CROS_SDK_BOARD} \
	--env num_jobs=${NUM_JOBS} \
	-v "$(realpath stage2/cros-sdk-build.sh):/home/cros/chromiumos/cros-sdk-build.sh" \
	"${image_name}-stage1:latest" \
	./cros-sdk-build.sh)
podman container logs -f ${id}
(exit $(podman container wait ${id}))
podman container commit ${id} "${image_name}-stage2:latest"
podman container rm ${id}

# Build the final image.
echo "Building final image for ${image_name}"

pushd stage3
podman build ${CACHE_OPTS}  -t "${image_name}:${image_tag}" \
	--build-arg cros_sdk_image="${image_name}-stage2:latest" \
	--build-arg num_jobs=${NUM_JOBS} \
	.
popd

# Clean up the intermediate stages.
echo "Cleaning up intermediate stages"

podman image rm "${image_name}-stage1:latest"
podman image rm "${image_name}-stage2:latest"

echo "Done, image available as ${image_name}:${image_tag}"
