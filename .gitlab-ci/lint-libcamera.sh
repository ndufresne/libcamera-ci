#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Lint libcamera

set -e

source "$(dirname "$0")/lib.sh"

libcamera_checkstyle() {
	echo "Running checkstyle for $CI_COMMIT_REF_NAME ($base..$CI_COMMIT_SHA)"

	utils/checkstyle.py $base..$CI_COMMIT_SHA
}

base=$(find_base origin/$CI_DEFAULT_BRANCH $CI_COMMIT_SHA)

if [[ $base == $CI_COMMIT_SHA ]] ; then
	echo "No commit to test, skipping"
	exit 0
fi

run libcamera_checkstyle
