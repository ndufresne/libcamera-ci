#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Run basic tests after native compilation

set -e

source "$(dirname "$0")/lib.sh"

libcamera_native_test() {
	echo "Running basic tests"

	# Test installation in system directory.
	meson install -C build --no-rebuild

	# Update the runtime linker cache.
	ldconfig

	# Run the cam application for basic testing, to verify that the binary
	# was properly linked and can be run. Test both the binary before
	# installation run from the source tree, and the binary installed in
	# /usr/local/.
	./build/src/apps/cam/cam -l
	cam -l
}

run libcamera_native_test
