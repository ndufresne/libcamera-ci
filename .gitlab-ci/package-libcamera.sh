#!/bin/bash

# SPDX-License-Identifier: GPL-2.0-or-later
# SPDX-FileCopyrightText: © 2023 Laurent Pinchart <laurent.pinchart@ideasonboard.com>
#
# Package libcamera binaries to publish them as artifacts

set -e

source "$(dirname "$0")/lib.sh"

libcamera_package() {
	echo "Package libcamera binaries"

	meson install -C build --no-rebuild \
		--destdir install \
		--tags bin,bin-devel,runtime

	tar -cJf libcamera-${CI_COMMIT_SHA}.tar.xz -C build/install/ .
}

run libcamera_package
